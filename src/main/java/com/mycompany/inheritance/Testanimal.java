/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.inheritance;

/**
 *
 * @author 66955
 */
public class Testanimal {
    public static void main(String[] args){
        Animal animal = new Animal("Ani","white",0);
        animal.speak();
        animal.walk();
        
        Dog dang = new Dog("Dang","black&White");
        dang.speak();
        dang.walk();
        
        Dog to = new Dog("To","Brown");
        to.speak();
        to.walk();

        Dog mome = new Dog("Mome","black&White");
        mome.speak();
        mome.walk();
        
        Dog bat = new Dog("Bat","black&White");
        bat.speak();
        bat.walk();
        
        Cat zero = new Cat("Zero","orange");
        zero.speak();
        zero.walk();
        
        Duck zom = new Duck("zom","orange");
        zom.speak();
        zom.walk();
        zom.fly();
        
        Duck gabgab = new Duck("GabGab","orange&black");
        gabgab.speak();
        gabgab.walk();
        gabgab.fly();
        
        System.out.println("zom is Animal: "+(zom instanceof Animal));
        System.out.println("zom is Duck: "+(zom instanceof Duck));
        System.out.println("zom is Cat: "+(zom instanceof Object));
        System.out.println("zom is Dog: "+(animal instanceof Dog));
        System.out.println("Animal is Animal: "+(zom instanceof Animal));
        
        Animal[] animals = {dang,to,mome,bat,zero,zom,gabgab};
        for(int i = 0;i<animals.length;i++){
            animals[i].walk();
            animals[i].speak();
            if(animals[i] instanceof Duck){
                Duck duck =(Duck)animals[i];
                duck.fly();
            }
        }
    }
        
        
}

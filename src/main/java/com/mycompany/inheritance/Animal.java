/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.inheritance;

/**
 *
 * @author 66955
 */
public class Animal {

    protected String name;
    protected int numberofLegs = 0;
    protected String color;

    public Animal(String name, String color, int numberofLegs) {
        System.out.println("Animal Created");
        this.name = name;
        this.color = color;
        this.numberofLegs = numberofLegs;
    }

    public void walk() {
        System.out.println("Animal walk");
    }

    public void speak() {
        System.out.println("Animal speak");
        System.out.println("name : " + this.name + " color : " + this.color + " legs : "
                + this.numberofLegs);
    }

    public String getName() {
        return name;
    }

    public String getnumberofLegs() {
        return name;
    }

    public String getColor() {
        return name;
    }

}
